// Find "s" on their first name Find "d" on their first name


db.users.find({ 
    $or: [
    { firstName: 
        { 
            $regex: 'S', 
            $options: '$i' 
        }
    }, 
    {lastName: 
        { 
            $regex: 'D', 
            $options: '$i' 
        } 
    }]}, {firstName: 1, lastName: 1, _id: 0})


// Find user who are in HR department

db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]})


// Find user with letter e in their first name and an age less than or equal to 30

db.users.find({ 
    $and: [ 
    {lastName: 
        { 
            $regex: 'E', 
            $options: '$i' 
        } 
    },
    {age: 
        {
            $lte: 30
        }
    }
    ]})